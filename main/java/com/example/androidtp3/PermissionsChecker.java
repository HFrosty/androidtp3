package com.example.androidtp3;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class PermissionsChecker {

    private final int REQUEST_PERMISSION_INTERNET_STATE = 1;

    Activity activity;

    public PermissionsChecker(Activity activity)
    {
        this.activity = activity;
    }

    public boolean hasPermissions()
    {
        if(ContextCompat.checkSelfPermission(activity, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED)
        {
            Log.e("PERMISSIONS", "APP DON'T HAVE INTERNET PERMISSION");
            return false;
        }
        Log.e("PERMISSIONS", "APP HAVE INTERNET PERMISSION");
        return true;
    }

    public void askPermissions()
    {
        ActivityCompat.requestPermissions(activity, new String[] {Manifest.permission.INTERNET}, REQUEST_PERMISSION_INTERNET_STATE);
    }

}
