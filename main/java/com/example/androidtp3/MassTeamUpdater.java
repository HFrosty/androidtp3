package com.example.androidtp3;

import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.net.URL;

public class MassTeamUpdater extends AsyncTask<URL, Integer, Long> {

    MainActivity activity;
    int updateCount;
    boolean allRequestsSent;
    int requestCount;

    public MassTeamUpdater(MainActivity activity)
    {
        this.updateCount = 0;
        this.requestCount = 0;
        this.activity = activity;
        this.allRequestsSent = false;
    }

    @Override
    protected Long doInBackground(URL... urls) {
        SportDbHelper db = new SportDbHelper(activity);
        Cursor cursor = db.fetchAllTeams();
        while(!cursor.isAfterLast())
        {
            Team currentTeam = db.cursorToTeam(cursor);
            Log.e("MassTeamUpdater", "Start refreshing team " + currentTeam.toString());
            TeamUpdater updater = new TeamUpdater(currentTeam, activity, this);
            updater.execute((URL) null);
            cursor.moveToNext();
            ++requestCount;
        }
        this.allRequestsSent = true;
        return Long.valueOf(0);
    }

    public void onFinishedUpdating() {
        ++updateCount;
        if(updateCount == requestCount && allRequestsSent)
        {
            SwipeRefreshLayout swipeResfreh = (SwipeRefreshLayout) activity.findViewById(R.id.swiperefresh);
            swipeResfreh.setRefreshing(false);
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    activity.resetList();
                }
            });
        }
    }
}
