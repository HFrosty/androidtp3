package com.example.androidtp3;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Debug;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class TeamAdapter extends RecyclerView.Adapter<TeamAdapter.TeamViewHolder> {

    public static class TeamViewHolder extends RecyclerView.ViewHolder {

        Team team;
        SportDbHelper db;
        MainActivity main;

        public TeamViewHolder(@NonNull View itemView, SportDbHelper db, MainActivity main) {
            super(itemView);
            team = null;
            this.db = db;
            this.main = main;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewHolderOnClick();
                }
            });
        }

        public void setTeam(Team team)
        {
            this.team = team;
            ((TextView) itemView.findViewById(R.id.teamName)).setText(team.getName());
            ((TextView) itemView.findViewById(R.id.leagueName)).setText(team.getLeague());
            ((TextView) itemView.findViewById(R.id.lastEvent)).setText(team.getLastEvent().getLabel());
            ImageViewDownloader imageDownloader = new ImageViewDownloader(team.getTeamBadge(), (ImageView) itemView.findViewById(R.id.teamBadge), main);
            imageDownloader.execute();
        }

        public void viewHolderOnClick()
        {
            Intent intent = new Intent(itemView.getContext(), TeamActivity.class);
            intent.putExtra(Team.TAG, team);
            itemView.getContext().startActivity(intent);
        }

        public void onSwipe()
        {
            db.deleteTeam((int) team.getId());
            main.resetList();
        }
    }

    List<Team> teams;
    MainActivity main;
    SportDbHelper db;

    public TeamAdapter(SportDbHelper db, MainActivity activity)
    {
        teams = db.getAllTeams();
        this.db = db;
        main = activity;
    }

    @NonNull
    @Override
    public TeamAdapter.TeamViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.team_list_element, parent, false);
        TeamViewHolder viewHolder = new TeamViewHolder(view, db, main);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull TeamViewHolder holder, int position) {
        Team team = teams.get(position);
        holder.setTeam(team);
    }

    @Override
    public int getItemCount() {
        return teams.size();
    }
}
