package com.example.androidtp3;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class ImageViewDownloader extends AsyncTask<URL, Integer, Long> {
    private final String url;
    private final ImageView imageView;
    private final Activity activity;

    public ImageViewDownloader(String url, ImageView imageView, Activity activity)
    {
        this.url = url;
        this.imageView = imageView;
        this.activity = activity;
    }

    @Override
    protected Long doInBackground(URL... urls) {
        Bitmap badge = null;
        if(url == null || url.equals(""))
        {
            return Long.valueOf(0);
        }
        try {
            badge = BitmapFactory.decodeStream(new URL(url).openStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        final Bitmap finalBadge = badge;
        this.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imageView.setImageBitmap(finalBadge);
            }
        });
        return Long.valueOf(0);
    }
}
