package com.example.androidtp3;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.net.URL;

public class MainActivity extends AppCompatActivity {

    SportDbHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PermissionsChecker checker = new PermissionsChecker(this);
        if(!checker.hasPermissions())
        {
            checker.askPermissions();
        }
        setContentView(R.layout.activity_main);
        db = new SportDbHelper(this);
        if(db.getAllTeams().size() == 0) {
            db.populate();
        }

        // Fill club list
        resetList();

        // Set what happens when user click on + floating button
        final FloatingActionButton plusButton = (FloatingActionButton) findViewById(R.id.fab);
        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToNewTeamActivity();
            }
        });

        final SwipeRefreshLayout refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateAllTeams();
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        // We reset the displayed list
        resetList();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && data.hasExtra(Team.TAG))
        {
            _addNewTeam((Team) data.getParcelableExtra(Team.TAG));
        }
    }

    public void resetList()
    {
        RecyclerView view = (RecyclerView) findViewById(R.id.clubList);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new TeamTouchHelperCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT));
        itemTouchHelper.attachToRecyclerView(view);
        TeamAdapter adapter = new TeamAdapter(db, this);
        view.setAdapter(adapter);
        view.setLayoutManager(new LinearLayoutManager(this));
    }

    private void _addNewTeam(Team team)
    {
        if(!db.addTeam(team))
        {
            new AlertDialog.Builder(MainActivity.this).setTitle("Ajout impossible").setMessage("Impossible de rajouter cette équipe à la base de données").show();
        }
    }

    public void goToNewTeamActivity()
    {
        Intent intent = new Intent(MainActivity.this, NewTeamActivity.class);
        Integer RESPONSE_CODE = 1;
        startActivityForResult(intent, RESPONSE_CODE);
    }

    public void updateAllTeams()
    {
        Log.e("MainActivity", "Refreshing all teams");
        new MassTeamUpdater(this).execute((URL) null);
    }
}
