package com.example.androidtp3;

import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseHandlerRanking {
    private static final String TAG = JSONResponseHandlerTeam.class.getSimpleName();

    private Team team;

    public JSONResponseHandlerRanking(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readTeams(reader);
        } finally {
            reader.close();
        }
    }

    public void readTeams(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("table")) {
                readArrayTeams(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayTeams(JsonReader reader) throws IOException {
        reader.beginArray();
        int rank = 1;
        while (reader.hasNext() ) {
            reader.beginObject();
            boolean canSetTotal = false;
            while (reader.hasNext()) {
                String name = reader.nextName();
                if(name.equals("teamid"))
                {
                    String teamId = reader.nextString();
                    if(Long.valueOf(teamId) == team.getIdTeam())
                    {
                        team.setRanking(rank);
                        canSetTotal = true;
                    }
                }
                else if(name.equals("total") && canSetTotal)
                {
                    team.setTotalPoints(reader.nextInt());
                    canSetTotal = false;
                }
                else
                {
                    reader.skipValue();
                }
            }
            ++rank;
            reader.endObject();
        }

        reader.endArray();
    }
}
