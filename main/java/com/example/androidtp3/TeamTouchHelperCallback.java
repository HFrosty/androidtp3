package com.example.androidtp3;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

public class TeamTouchHelperCallback extends ItemTouchHelper.SimpleCallback {

    public TeamTouchHelperCallback(int dragDirs, int swipeDirs) {
        super(dragDirs, swipeDirs);
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
        TeamAdapter.TeamViewHolder holder = (TeamAdapter.TeamViewHolder) viewHolder;
        holder.onSwipe();
        Log.d("SWIPED", "THATS THE WRONG NUMBER");
    }
}
