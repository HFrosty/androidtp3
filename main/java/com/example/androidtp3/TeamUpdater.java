package com.example.androidtp3;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class TeamUpdater extends AsyncTask<URL, Integer, Long> {

    Team team;
    TeamActivity teamActivity;
    MainActivity mainActivity;
    MassTeamUpdater massUpdater;

    // If activity is null, it means we are not updating in the Team Activity, therefore, updateView() is not called after the data fetching
    public TeamUpdater(Team team, TeamActivity activity)
    {
        this.team = team;
        this.teamActivity = activity;
        this.massUpdater = null;
    }

    public TeamUpdater(Team team, MainActivity activity, MassTeamUpdater massUpdater)
    {
        this.massUpdater = massUpdater;
        this.mainActivity = activity;
        this.team = team;
        this.teamActivity = null;
    }

    @Override
    protected Long doInBackground(URL... urls) {
        WebServiceUrl webService = new WebServiceUrl();
        InternetDownloader downloader = null;
        try {
            downloader = new InternetDownloader(webService.buildSearchTeam(team.getName()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        JSONResponseHandlerTeam handlerTeam = new JSONResponseHandlerTeam(team);
        try {
            handlerTeam.readJsonStream(downloader.Download());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            downloader = new InternetDownloader(webService.buildGetRanking(team.getIdLeague()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        JSONResponseHandlerRanking rankHandler = new JSONResponseHandlerRanking(team);
        try {
            rankHandler.readJsonStream(downloader.Download());
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONResponseHandlerLastEvent matchHandler = new JSONResponseHandlerLastEvent(team);
        try {
            downloader = new InternetDownloader(webService.buildSearchLastEvents(team.getIdTeam()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            matchHandler.readJsonStream(downloader.Download());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.e("TeamUpdater", "Finshed updating team " + team.toString());
        if(teamActivity != null)
        {
            new SportDbHelper(teamActivity).updateTeam(team);
            teamActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    teamActivity.updateView();
                }
            });
        }

        if(massUpdater != null)
        {
            new SportDbHelper(mainActivity).updateTeam(team);
            massUpdater.onFinishedUpdating();
        }
        return Long.valueOf(0);
    }
}
