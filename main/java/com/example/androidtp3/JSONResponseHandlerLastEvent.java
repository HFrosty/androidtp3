package com.example.androidtp3;

import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseHandlerLastEvent {
    private static final String TAG = JSONResponseHandlerTeam.class.getSimpleName();

    private Team team;

    public JSONResponseHandlerLastEvent(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readTeams(reader);
        } finally {
            reader.close();
        }
    }

    public void readTeams(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("results")) {
                readArrayTeams(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayTeams(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 0; // only consider the first element of the array
        Match match = new Match();
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (nb==0) {
                    if (name.equals("strEvent")) {
                        match.setLabel(reader.nextString());
                    } else if (name.equals("idEvent")) {
                        match.setId(Long.valueOf(reader.nextString()));
                    } else if (name.equals("strHomeTeam")) {
                        match.setHomeTeam(reader.nextString());
                    } else if (name.equals("strAwayTeam")) {
                        match.setAwayTeam(reader.nextString());
                    } else if (name.equals("intHomeScore")) {
                        if(reader.peek() == JsonToken.NULL)
                        {
                            match.setHomeScore(0);
                            reader.nextNull();
                        }
                        else
                        {
                            match.setHomeScore(Integer.valueOf(reader.nextString()));
                        }

                    } else if (name.equals("intAwayScore")) {
                        if(reader.peek() == JsonToken.NULL)
                        {
                            match.setAwayScore(0);
                            reader.nextNull();
                        }
                        else
                        {
                            match.setAwayScore(Integer.valueOf(reader.nextString()));
                        }
                    } else {
                        reader.skipValue();
                    }
                }  else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
        team.setLastEvent(match);
    }
}
